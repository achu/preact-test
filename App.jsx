import { h, render } from "preact";
import { Suspense, lazy } from "preact/compat";

const Content = lazy(() => import("./Content"));

const App = () => (
  <Suspense fallback={<div>loading...</div>}>
    <Content />
  </Suspense>
);
render(<App />, document.body);
